﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Library
{
    public class TextValidator
    {
        private bool NumberValidator(TextBox textBox)
        {
            var rg = new Regex(@"^[0-9]\d*(\.\d+)?$");

            return rg.IsMatch(textBox.Text);
        }

        public void ValidateNumberInTextBox(object sender, EventArgs e)
        {
            var answerBox = (TextBox)sender;

            var response = NumberValidator(answerBox);

            if (response == false)
            {
                MessageBox.Show("You must enter a positive number.", "Error");
                return;
            }
        }
    }
}