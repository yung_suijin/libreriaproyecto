﻿using System.Windows.Forms;

namespace Library
{
    public class DragAndDropEvents
    {
        private static int successCount;
        private readonly WindowEvents windowEvents = new WindowEvents();

        public void DragEnterImage(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        public void ImageMouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                var pictureBox = (PictureBox)sender;

                if (pictureBox.BackgroundImage != null)
                {
                    pictureBox.DoDragDrop(pictureBox, DragDropEffects.Move);
                }
            }
        }

        public void SelectedImage(PictureBox pictureBox, PictureBox[] images, PictureBox selected)
        {
            if (selected != pictureBox)
                selected = pictureBox;
            else
                selected = null;

            foreach (var image in images)
                image.Invalidate();
        }

        public void DropAndCheck(PictureBox lengthUnit, PictureBox answerBox, Form currentWindow,
            Form newWindow)
        {
            if (lengthUnit.BackgroundImage == null && answerBox.BackgroundImage == null)
                return;

            if (lengthUnit.Tag != answerBox.Tag)
            {
                MessageBox.Show("You choosed the wrong length unit to measure this one.", "Error");
                return;
            }

            answerBox.BackgroundImage = lengthUnit.BackgroundImage;

            successCount++;

            if (successCount == 7)
                windowEvents.CloseCurrentWindowAndOpenNextWindow(currentWindow, newWindow);
        }
    }
}