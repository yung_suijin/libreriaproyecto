﻿using System.Windows.Forms;

namespace Library
{
    public class WindowEvents
    {
        public void CloseCurrentWindowAndOpenNextWindow(Form currentWindow, Form nextWindow)
        {
            currentWindow.Hide();

            nextWindow.Closed += (s, args) => currentWindow.Close();

            nextWindow.Show();
        }

        public void MaintainCurrentWindowOpenAndOpenNextWindow(Form nextWindow)
        {
            nextWindow.Show();
        }
    }
}