﻿using System;
using System.Windows.Forms;

namespace Library
{
    public class TextBoxAnswersEvents
    {
        private static int correctAnswers;
        private readonly WindowEvents windowEvents = new WindowEvents();

        public void CheckTextBoxAnswers(Button[] checkButtons, Button clickedButton, TextBox[] answerBoxes, 
            Form currentWindow, Form newWindow)
        {
            int index = Array.IndexOf(checkButtons, clickedButton);

            if (answerBoxes[index].Text.Equals(clickedButton.Tag))
            {
                MessageBox.Show("Correct answer.", "Success");

                clickedButton.Enabled = false;
                answerBoxes[index].Enabled = false;
                correctAnswers++;

                if (correctAnswers == 6)
                    windowEvents.CloseCurrentWindowAndOpenNextWindow(currentWindow, newWindow);

                return;
            }
            else
            {
                MessageBox.Show("Your answer is incorrect.", "Error");
                return;
            }
        }
    }
}